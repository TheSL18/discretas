package co.edu.unbosque.view;
import java.util.*;

//Clase VistaConsola
public class VistaConsola {
	private Scanner leer;
	public VistaConsola() {
		leer = new Scanner(System.in);
	}
	
	public int leerDato(String mensaje) {
		System.out.println(mensaje);
		int dato = leer.nextInt();
		leer.nextLine();
		return dato;
	}
	
	public void mostrarInformacion(String mensaje) {
		System.out.println(mensaje);
	}
}
