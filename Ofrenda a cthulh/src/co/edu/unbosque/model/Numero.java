package co.edu.unbosque.model;

//Clase Numero
public class Numero {
	private int n;

//Constructor	
	public Numero() {
		n = 0;
	}

//Constructor_con_parametos	
	public Numero(int n) {
		this.n = n;
	}

// Numeros_primos
	public String primos() {
		int nn = n;
		String resp = "";
		if (nn > 1)
			for (int i = 2; i <= nn; ++i)
				while (nn % i == 0) {
					resp += "\n" + nn + " | " + i + "\n";
					nn /= i;
				}
		else
			resp = " no pueden ser encontrados porque usted ingreso un dato  erroneo";
		return resp;
	}

//potencias+contador
	public String potencias(Numero b) {
		String resp = "";
		int nn = n;
		if (nn > 1)
			for (int i = 2; i <= nn; ++i) {
				int contador = 0;
				while (nn % i == 0) {
					++contador;
					nn /= i;
				}
				if (nn == 1) {
					if (contador > 1)
						resp += i + " ^ " + contador;
					else if (contador == 1)
						resp += i;
				} else {
					if (contador > 1)
						resp += i + " ^ " + contador + " x ";
					else if (contador == 1)
						resp += i + "   * ";
				}
			}
		return resp;
	}

//axioma
	public String axioma(Numero c) {
		String resp = "";
		int resultado = 1;
		int nn = n;
		if (nn > 1)
			for (int i = 2; i <= nn; ++i) {
				int contador = 0;
				while (nn % i == 0) {
					++contador;
					nn /= i;
				}
				if (nn == 1) {
					if (contador > 1)
						resp += "(" + contador + " +1)";
					else if (contador == 1)
						resp += "(1+1) ";
				} else {
					if (contador > 1)
						resp += " (" + contador + " +1)";
					else if (contador == 1)
						resp += " (1 +1) ";
				}
				resultado *= contador + 1;
			}
		return " CDD (" + n + ") = " + resp + "=" + resultado;
	}

//Getter	
	public int getN() {
		return n;
	}

//Setter	
	public void setN(int n) {
		this.n = n;
	}
}